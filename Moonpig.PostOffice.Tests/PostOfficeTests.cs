﻿using Moonpig.PostOffice.Common.Constants;
using Moonpig.PostOffice.Common.Exceptions;
using Moonpig.PostOffice.Data;
using Moonpig.PostOffice.Domain.Interfaces;
using Moonpig.PostOffice.Domain.Validators;
using Moq;

namespace Moonpig.PostOffice.Tests
{
    using System;
    using System.Collections.Generic;
    using Domain;
    using Xunit;

    public class PostOfficeTests
    {
        private readonly PostOffice _sut;
        private readonly IDbContext _dbContext;

        public PostOfficeTests()
        {
            var despatchCalculatorMock = new Mock<IDespatchCalculator>();
            _dbContext = new DbContext();
            var orderValidatorFactoryMock = new Mock<IOrderValidatorFactory>();
            var orderValidatorMock = new Mock<IOrderValidator>();

            orderValidatorFactoryMock.Setup(o => o.Create(It.IsAny<OrderValidationLevel>()))
                .Returns(orderValidatorMock.Object);

            orderValidatorMock
                .Setup(o => o.Validate(It.IsAny<Order>()))
                .Returns(true);

            _sut = new PostOffice(despatchCalculatorMock.Object, _dbContext, orderValidatorFactoryMock.Object);
        }

        [Fact]
        public void Fail_when_order_has_unknown_product()
        {
            const int productId = -1;
            var order = new Order(new List<int>() {productId}, DateTime.Now);

            var exception = Assert
                .Throws<DespatchDateCalculationException>(() => _sut.CalculateDespatchDate(order));
            Assert.Equal(exception.Message, string.Format(ValidationErrors.ProductNotFound, productId));
        }

        [Fact]
        public void Fail_when_supplier_does_not_exist_for_a_product()
        {
            const int unknownSupplierId = -1;
            var productId  = _dbContext.AddProduct(new Product
            {
                Name = "test",
                SupplierId = unknownSupplierId
            });
            
            var order = new Order(new List<int>() {productId}, DateTime.Now);

            var exception = Assert
                .Throws<DespatchDateCalculationException>(() => _sut.CalculateDespatchDate(order));
            Assert.Equal(exception.Message, string.Format(ValidationErrors.SuppliertNotFound, unknownSupplierId));
        }
    }
}
