﻿using System;
using Moonpig.PostOffice.Common.Constants;
using Moonpig.PostOffice.Domain.Validators;
using Xunit;
using Shouldly;

namespace Moonpig.PostOffice.Tests
{
    public class OrderValidatorFactoryTests
    {
        private readonly OrderValidatorFactory _sut;

        public OrderValidatorFactoryTests()
        {
            _sut = new OrderValidatorFactory();
        }

        [Fact]
        public void Fail_when_validator_is_not_fount()
        {
            const OrderValidationLevel validatiohnLEvel = OrderValidationLevel.None;
            var exception = Assert
                .Throws<NotImplementedException>(() => _sut.Create(validatiohnLEvel));
            Assert.Equal(exception.Message, string.Format(ValidationErrors.UnSupportedValidationLeven, validatiohnLEvel));
        }

        [Fact]
        public void It_should_return_a_correct_validator()
        {
            const OrderValidationLevel validatiohnLEvel = OrderValidationLevel.Preliminary;
            var validator = _sut.Create(validatiohnLEvel);
            validator.ShouldBeOfType<PreliminaryOrderValiadator>();
        }
    }
}
