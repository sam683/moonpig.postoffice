﻿using System;
using System.Collections.Generic;
using Moonpig.PostOffice.Common.Constants;
using Moonpig.PostOffice.Common.Exceptions;
using Moonpig.PostOffice.Domain;
using Moonpig.PostOffice.Domain.Validators;
using Xunit;
using Shouldly;


namespace Moonpig.PostOffice.Tests
{
    public class PreliminaryOrderValiadatorTests
    {
        private readonly PreliminaryOrderValiadator _sut;

        public PreliminaryOrderValiadatorTests()
        {
            _sut = new PreliminaryOrderValiadator();
        }

        [Fact]
        public void Fail_when_roder_request_is_null()
        {
            var exception = Assert
                .Throws<OrderValidationException>(() => _sut.Validate(null));
            exception.Message.ShouldBe(ValidationErrors.EmptyOrder);            
        }

        [Fact]
        public void Fail_when_roder_date_is_in_the_future()
        {
            var order = new Order(new List<int>() {1}, DateTime.Now.AddDays(1).ToUniversalTime());
            var exception = Assert
                .Throws<OrderValidationException>(() => _sut.Validate(order));
            exception.Message.ShouldBe(ValidationErrors.OrderDateInFuture);            
        }

        [Fact]
        public void Fail_when_roder_has_null_or_empty_products()
        {
            var order = new Order(new List<int>() {}, DateTime.Now.ToUniversalTime());
            var exception = Assert
                .Throws<OrderValidationException>(() => _sut.Validate(order));
            exception.Message.ShouldBe(ValidationErrors.EmptyOrder);            
        }

        [Fact]
        public void Validation_should_return_true_for_a_valid_order()
        {
            var order = new Order(new List<int>() {1}, DateTime.Now.ToUniversalTime());
            var result = _sut.Validate(order);
            result.ShouldBe(true);            
        }
    }
}
