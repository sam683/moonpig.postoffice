﻿using System;
using Moonpig.PostOffice.Domain.DespatchCalculators;
using Xunit;
using Shouldly;

namespace Moonpig.PostOffice.Tests
{
    public class DespatchCalculatorTests
    {
        private readonly DespatchCalculator _sut;

        public DespatchCalculatorTests()
        {
            _sut = new DespatchCalculator();
        }

        [Fact]
        public void OneProductWithLeadTimeOfOneDay()
        {
            var orderDate = new DateTime(2018, 9, 17);
            var date = _sut.Calculate(orderDate, 1);
            date.DayOfWeek.ShouldBe(DayOfWeek.Tuesday);
        }

        [Fact]
        public void OneProductWithLeadTimeOfTwoDay()
        {
            var orderDate = new DateTime(2018, 9, 17);
            var date = _sut.Calculate(orderDate, 2);
            date.DayOfWeek.ShouldBe(DayOfWeek.Wednesday);
        }

        [Fact]
        public void OneProductWithLeadTimeOfThreeDay()
        {
            var orderDate = new DateTime(2018, 9, 17);
            var date = _sut.Calculate(orderDate, 3);
            date.DayOfWeek.ShouldBe(DayOfWeek.Thursday);
        }

        [Fact]
        public void SaturdayHasExtraTwoDays()
        {
            var orderDate = new DateTime(2018, 1, 26);
            var date = _sut.Calculate(orderDate, 3);
            date.DayOfWeek.ShouldBe(DayOfWeek.Wednesday);
        }

        [Fact]
        public void SundayHasExtraDay()
        {
            var orderDate = new DateTime(2018, 1, 25);
            var date = _sut.Calculate(orderDate, 3);
            date.DayOfWeek.ShouldBe(DayOfWeek.Tuesday);
        }
    }
}
