﻿using System;

namespace Moonpig.PostOffice.Common.Exceptions
{
    public class OrderValidationException:
        Exception
    {
        public OrderValidationException(string message):
            base(message)
        {
        }
    }
}
