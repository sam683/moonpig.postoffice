﻿
using System;

namespace Moonpig.PostOffice.Common.Exceptions
{
    public class DespatchDateCalculationException:
        Exception
    {
        public DespatchDateCalculationException(string message):
            base(message)
        {
        }
    }
}
