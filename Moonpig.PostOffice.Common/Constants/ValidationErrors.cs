﻿namespace Moonpig.PostOffice.Common.Constants
{
    public static class ValidationErrors
    {
        public const string ProductNotFound =
            "Could not find the product with id={0}.";
        public const string SuppliertNotFound =
            "Could not find any Supplier with id={0}.";
        public const string UnSupportedValidationLeven = 
            "Could not find any validator for this validationLevel {0}.";
        public const string EmptyOrder = 
            "Order items can not be null or empty.";   
        public const string OrderDateInFuture = 
            "Order date can not be in the future.";
    }
}
