﻿using System.Collections.Generic;
using Moonpig.PostOffice.Common.Constants;
using Moonpig.PostOffice.Common.Exceptions;
using Moonpig.PostOffice.Domain.Interfaces;
using Moonpig.PostOffice.Domain.Validators;

namespace Moonpig.PostOffice.Domain
{
    using System;
    using System.Linq;
    using Data;

    public class PostOffice : IPostOffice
    {
        private readonly IDespatchCalculator _despatchCalculator;
        private readonly IDbContext _dbContext;
        private readonly IOrderValidatorFactory _orderValidatorFactory;
        public PostOffice(IDespatchCalculator despatchCalculator, IDbContext dbContext, 
            IOrderValidatorFactory orderValidatorFactory)
        {
            _despatchCalculator = despatchCalculator;
            _dbContext = dbContext;
            _orderValidatorFactory = orderValidatorFactory;
        }

        public DateTime CalculateDespatchDate(Order order)
        {
            var validator = _orderValidatorFactory.Create(OrderValidationLevel.Preliminary);

            validator.Validate(order);

            var orderDate = order.OrderDate;
            var leadTimes = new List<int>();
            foreach (var id in order.ProductIds)
            {
                var supplierId = _dbContext
                    .Products
                    .SingleOrDefault(x => x.ProductId == id)?
                    .SupplierId;
                if (supplierId == null)
                {
                    throw new DespatchDateCalculationException
                        (string.Format(ValidationErrors.ProductNotFound, id));
                }

                var leadTime = _dbContext
                    .Suppliers
                    .SingleOrDefault(x => x.SupplierId == supplierId)?
                    .LeadTime;

                if (leadTime == null)
                {
                    throw new DespatchDateCalculationException
                        (string.Format(ValidationErrors.SuppliertNotFound, supplierId));
                }

                leadTimes.Add(leadTime.Value);
            }

            return _despatchCalculator.Calculate(orderDate, leadTimes.Max());
        }
    }
}