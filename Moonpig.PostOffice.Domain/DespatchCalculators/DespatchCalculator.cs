﻿using System;
using Moonpig.PostOffice.Domain.Interfaces;

namespace Moonpig.PostOffice.Domain.DespatchCalculators
{
    public class DespatchCalculator : IDespatchCalculator
    {
        public DateTime Calculate(DateTime orderDate, int longestLeadTime)
        {
            while (longestLeadTime > 0)
            {
                orderDate = orderDate.AddDays(1);
                if (orderDate.DayOfWeek == DayOfWeek.Saturday
                    || orderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    orderDate = orderDate.AddDays(1);
                    continue;
                }

                longestLeadTime--;
            }

            return orderDate;
        }
    }
}
