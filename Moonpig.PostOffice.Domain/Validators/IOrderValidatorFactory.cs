﻿namespace Moonpig.PostOffice.Domain.Validators
{
    public interface IOrderValidatorFactory
    {
        IOrderValidator Create(OrderValidationLevel validationLevel);
    }
}
