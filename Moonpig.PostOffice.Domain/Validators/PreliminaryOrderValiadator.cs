﻿using System;
using System.Linq;
using Moonpig.PostOffice.Common.Constants;
using Moonpig.PostOffice.Common.Exceptions;

namespace Moonpig.PostOffice.Domain.Validators
{
    public class PreliminaryOrderValiadator : IOrderValidator
    {
        public bool Validate(Order order)
        {
            if (order == null)
            {
                throw new OrderValidationException(ValidationErrors.EmptyOrder);
            }

            var today = DateTime.Now.ToUniversalTime();

            if (order.OrderDate.ToUniversalTime() > today)
            {
                throw new OrderValidationException(ValidationErrors.OrderDateInFuture);
            }

            if (!order.ProductIds.Any())
            {
                throw new OrderValidationException(ValidationErrors.EmptyOrder);
            }
            
            return true;
        }
    }
}
