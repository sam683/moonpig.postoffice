﻿using System;
using Moonpig.PostOffice.Common.Constants;

namespace Moonpig.PostOffice.Domain.Validators
{
    public class OrderValidatorFactory : IOrderValidatorFactory
    {
        public IOrderValidator Create(OrderValidationLevel validationLevel)
        {
            switch (validationLevel)
            {
                case OrderValidationLevel.Preliminary:
                    return new PreliminaryOrderValiadator();
                default:
                    throw new NotImplementedException(string.Format
                        (ValidationErrors.UnSupportedValidationLeven, validationLevel));
            }
        }
    }
}
