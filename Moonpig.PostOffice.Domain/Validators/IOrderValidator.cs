﻿namespace Moonpig.PostOffice.Domain.Validators
{
    public interface IOrderValidator
    {
        bool Validate(Order order);
    }
}
