﻿namespace Moonpig.PostOffice.Domain.Validators
{
    public enum OrderValidationLevel
    {
        None = 0,
        Preliminary = 1,
    }
}
