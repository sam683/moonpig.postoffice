﻿using System;

namespace Moonpig.PostOffice.Domain.Interfaces
{
    public interface IPostOffice
    {
        DateTime CalculateDespatchDate(Order order);
    }
}
