﻿using System;

namespace Moonpig.PostOffice.Domain.Interfaces
{
    public interface IDespatchCalculator
    {
        DateTime Calculate(DateTime orderDate, int longestLeadTime);
    }
}
